class Card {
    constructor(name,email,title,body,postId){
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.postId = postId;
    }
}


const URL = 'https://ajax.test-danit.com/api/json/';
const posts = document.querySelector('.posts')

const sendRequest = async (entity, method = "GET", config) => {
    return await fetch(`${URL}${entity}`, {
        method,
        ...config
    }).then(response => {
            return response.json()
    })
} 

const getUsers =  () =>  sendRequest('users');
const getPost =  () =>  sendRequest(`posts`)
const deletePost =  (id) =>  sendRequest(`posts/${id}`, "DELETE")




Promise.all([getUsers(),getPost()]).then(data =>{
    const usersUrl = data[0];
    const postsUrl = data[1];
    usersUrl.forEach(({name,username,email}) => {
        postsUrl.forEach(({title,body,id})=>{
            const postCard = document.createElement("div");
            postCard.classList.add('post__card')
            posts.append(postCard)

            const user = document.createElement("div");
            user.classList.add('user')
            postCard.append(user)


            const userInfo = document.createElement("div");
            userInfo.classList.add('user__info');
            user.append(userInfo);

            const userEmail = document.createElement("div");
            userEmail.classList.add('user__email');
            userEmail.innerHTML = `${email}`;
            user.append(userEmail);

            const userName = document.createElement("div");
            userName.classList.add('user__name');
            userName.innerHTML = `${name}`;
            userInfo.append(userName);

            const userNickname = document.createElement("div");
            userNickname.classList.add('user__nickname');
            userNickname.innerHTML = `${username}, Post: ${id}`;
            userInfo.append(userNickname);

            

            const post = document.createElement("div");
            post.classList.add('post');
            postCard.append(post);


            const postTitle = document.createElement("h2");
            postTitle.classList.add('post__title');
            postTitle.innerHTML = `${title}`;
            post.append(postTitle);

            const postText = document.createElement("p");
            postText.classList.add('post__text');
            postText.innerHTML = `${body}`;
            post.append(postText);




            const postRemove = document.createElement("BUTTON");
            postRemove.classList.add('post__remove');
            postRemove.innerHTML = `DELETE`;
            post.append(postRemove);


            
            postRemove.addEventListener('click',()=>{
            deletePost(id)
            const card = document.querySelector('.post__card');
            card.remove()
                alert('Post Deleted!!!')
            })
        })
   
    });
})





